#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

void allEvens(Array& evens, Array& array, int evenSize, int arraySize)
{
    Context _("allEvens", evenSize, arraySize); // do not care about this, it allow the display of call stack

    // your code
    // ça marche !!!
    if(arraySize==1 && array[0]%2==0)//si le tableau dans lequel on cherche notre seul valeur est paire on l'ajoute
    {
        evens.insert(0,array[0]);
        evenSize++;
    }
    else{//si on a un tableau de plus de 1 on va vérifier à partir de la dernière valeur du tableau à arraySize-1 si c'est paire
        if(array[arraySize-1]%2==0){
            evens[evenSize]=array[arraySize--];
            evenSize++;
            allEvens(evens, array, evenSize, arraySize--);//
            //on va rappeler la fonction pour rechercher si la dernière valeur du tableau est paire sachant que la dernière valeur du tableau a changé puisqu'on a réduit la taille de arraySize de 1
        } 
        else {
            allEvens(evens, array, evenSize, arraySize--);
        }
    }
    
    return;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new AllEvensWindow(allEvens); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




