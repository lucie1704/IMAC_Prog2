#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());

	// insertion sort from toSort to sorted
	// ça marche !!

	sorted[0]=toSort[0]; //on copie la première valeur pour avoir un truc avec lequel comparer, on commence le trie en fonction de ce premier élément

	for (int i = 1; i < toSort.size(); i++) //parcourt toSort
    {
		int index=0;
		//on cherche l'indice ou insérer la valeur de toSort[]
		for (int j=i-1; j>-1; j--){ //on parcourt sorted du dernier élément vers le premier
			if(toSort[i]>sorted[j]){
				index=j+1; //on indique l'indice qui suit s'il est plus grand
				break; //le break nous permet de sortir de la boucle 
			}
		}
		sorted.insert(index, toSort[i]);//j'insert la valeur au bon indice
	}
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
