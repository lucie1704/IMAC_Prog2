#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
	// ça marche 
	int n =toSort.size();
	for (int i = 0; i <n-1; i++)//on parcourt toSort
	{
		for (int j = 0; j<n-1-i; j++)//n-i-1 car quand on a bouclé 2 fois dans la boucle d'avant soit i=1, on a déjà 2 valeurs triées à la fin du tableau qu'on ne veut pas recomparer
		{	
			if(toSort[j+1]<toSort[j]){//si la valeur du tableau adjacente est plus petite que celle ou on se trouve on swap
				toSort.swap(j, j+1); //swap avec les indices
			}
		}
	}
	

}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 300;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
